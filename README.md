![](acsharp-docker.png)


# ACSharp Docker

This is the recommended method of running a ACSharp.

#### Prerequisites

- Ubuntu is recommended (other Debian based distributions should work out of the box). Any distro with Docker support should work.

#### Install

Log in as root.

```
apt update && apt -y install git telnet net-tools
git clone https://gitlab.com/logicethos/acsharp-docker.git acsharp
cd acsharp
./scripts/install-ubuntu.sh <-- this will install the latest verion of Docker.
```

Note: The firewall assumes ssh access is on port 22. Read the firewall section if this is not the case. Failing to do so, will lock you out of your server. If you have an external firewall, you will need to open ports for ACSharp to operate (see below).

#### Run & Configure

All operations are done, using the run command.

```
./acsharp
```

If you want to create a ssh login account that takes you directly to the ACSharp Manager menu:

```
./acsharp createuser acsharp
```

This will create a login account 'acsharp'. The private keys from ~/.ssh/authorized_keys will be copied to /home/acsharp-docker/.ssh/authorized_keys. Edit as necessary.

---

#### Firewall & Rate Limiting


*It is still your responsibility to make sure your server is secure.


#### SSL certificates

A self-certificate is automatically generated on start-up.
If you have an existing certificate for a domain name, copy the `.crt` and `.key` files to the `./certs` directory. They will be read on start-up. `.pem` files are also compatible. Rename them to `.crt` and `.key` files respectively. Set up a cron job, to make sure the files are kept valid.  These files will be monitored for changes, and automatically reloaded.
If you provide an e-mail address, ACSharp Manager will attempt to get a certificate from LetsEncrypt, and keep it renewed.
