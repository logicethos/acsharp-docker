#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


if command -v docker &> /dev/null
then
    echo "============================================================="
    echo "WARNING:  Docker is already installed on this OS."
    echo "This script may make unwanted changes to Docker."
    echo ""
    read -p "Continue installation? " -n 1 -r
     if [[ $REPLY =~ ^[Nn]$ ]]
     then
        echo ""
        exit 1
     fi 
fi



# Get Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update

#Remove old version
apt remove docker docker-engine docker.io containerd runc

#install basics
apt install -y \
    ca-certificates \
    curl wget\
    gnupg \
    lsb-release \
    jq

apt install -y docker-ce docker-ce-cli containerd.io

echo "Done"